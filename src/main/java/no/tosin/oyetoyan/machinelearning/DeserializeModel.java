/*
 * JiraSecPlugin 
 * Copyright 2016 Tosin Daniel Oyetoyan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package no.tosin.oyetoyan.machinelearning;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;

import no.tosin.oyetoyan.plugins.PropertyKeys;
import no.tosin.oyetoyan.plugins.SecurityImportance;
import no.tosin.oyetoyan.plugins.SecurityKeyWords;
import no.tosin.oyetoyan.plugins.SecurityMessage;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

/**
 * @author tosindo
 *
 */
public class DeserializeModel {
	
	private static Classifier model = null;
	private static Evaluation modelEval;
	private ProcessDataset processData;
	
	private static final String LEARNER = "ml";
	private static final String TSEARCH = "ts";
	
	private static Properties prop;
	private static SecurityKeyWords skw;
	private String propValue;
	
	private String message;
	private String importance;
	
	private static final Logger log = LoggerFactory.getLogger(DeserializeModel.class);
	
	//make it a singleton class
	private static DeserializeModel INSTANCE = null;		//best to use final here but no need bcos we don't have synchronization problem
	
	private DeserializeModel(SecurityKeyWords seckeywords){
		skw = seckeywords;
		prop = skw.getPropertyValues();
		processData = new ProcessDataset(skw);
		propValue = prop.getProperty(PropertyKeys.LEARNER_TSEARCH);
		if(propValue.equals(LEARNER))
			loadModel();
	}
	
	public static DeserializeModel getInstance(SecurityKeyWords skw){
		if(INSTANCE == null)
			INSTANCE = new DeserializeModel(skw);
		return INSTANCE;
	}
	
	private void loadModel(){
		//load model by deserializing model object	
		if(model == null){
			String MODEL_PATH = "";
			if(skw.isProduction())
				MODEL_PATH = ComponentAccessor.getComponentOfType(JiraHome.class).getHome()+
				System.getProperty("file.separator")+PropertyKeys.FOLDER+System.getProperty("file.separator");
			else
				MODEL_PATH = PropertyKeys.STORAGE_PATH_TEST;
			try (
					FileInputStream fis = new FileInputStream(MODEL_PATH+"model.cls");
					ObjectInputStream ois = new ObjectInputStream(fis);
					FileInputStream fise = new FileInputStream(MODEL_PATH+"modeleval.cls");
					ObjectInputStream oise = new ObjectInputStream(fise);
				)
			{
						
				model = (Classifier) ois.readObject();
				modelEval = (Evaluation) oise.readObject();
			} catch (FileNotFoundException e) {
				log.info(e.getMessage());
				propValue = TSEARCH;
			}catch(IOException | ClassNotFoundException e){
				log.info(e.getMessage());
				propValue = TSEARCH;
			}
		}
		
	}
	
	/**
	 * @param text	text to classify
	 * @return predicted class for the supplied text
	 */
	public String classifyText(String text){
		
		String predicted = null;
		
		processData.addDatasetInstance(text, "0");
		processData.generateFeatureSpace();
		
		if(propValue.equals(LEARNER)){
			Instances instances = processData.buildDataset();
			//System.out.println(instances.get(0));
			double pred = 0;
			try {
				pred = model.classifyInstance(instances.get(0));
				//message = text+"\nUsing AI Model-Result: "+pred;
			} catch (Exception e) {
				log.error(e.getMessage()); 
			}
			
			predicted = instances.classAttribute().value((int) pred);
			
			if(predicted.equals("1"))
				predicted = prop.getProperty(PropertyKeys.OPTION_YES);
			else
				predicted = prop.getProperty(PropertyKeys.OPTION_NO);
			

		}else{
			if(processData.getNumTermFound() > 0)
				predicted = prop.getProperty(PropertyKeys.OPTION_YES);
			else
				predicted = prop.getProperty(PropertyKeys.OPTION_NO);
			
			//message = text+"\nUsing Ordinary String Search-Result: "+predicted;
		}
		
		message = new SecurityMessage(processData.getAssetTermsFound(), processData.getControlTermsFound(),
				processData.getDirectTermsFound(), processData.getIndirectTermsFound(), skw).getMessage();
		SecurityImportance secImp = new SecurityImportance(prop, skw);
		secImp.compute(processData.getAssetTermsFound().size(), processData.getControlTermsFound().size(),
				processData.getDirectTermsFound().size(), processData.getIndirectTermsFound().size());
		importance = secImp.getImportance();
		
		return predicted;
	}
	
	public void printAttributes(){
		System.out.println(processData.getFeatures());
	}
	
	public void printModelPerformance(){
		try {
			System.out.println(modelEval.toSummaryString());
			System.out.println(modelEval.toMatrixString());
			System.out.println(modelEval.toClassDetailsString());
		} catch (Exception e) {
			// 
		}
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
	public int getNumTermFound(){
		return processData.getNumTermFound();
	}
	
	/**
	 * @return the number of attributes
	 */
	public int getNumAttributes() {
		return processData.getFeatures().size();
	}

	/**
	 * @return the importance
	 */
	public String getImportance() {
		return importance;
	}

}

/**
JiraSecPlugin 
Copyright 2016 Tosin Daniel Oyetoyan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package no.tosin.oyetoyan.plugins;


public final class PropertyKeys {
	
	/**
	 * These keys are defined in the "security-issue-classifier.properties" file under resources. Any modification to the values here should be
	 * manually updated in this property file. 
	 */
	public static final String SECURITY_ISSUE = "sec_issue";	
	public static final String SECURITY_IMPORTANCE = "sec_importance";
	public static final String SECURITY_DESC = "sec_desc";
	
	public static final String METHOD = "method";
	public static final String LEVENSHTEIN_THRESHOLD = "levenshtein_threshold";
	public static final String JAROWINKLER_THRESHOLD = "jarowinkler_threshold";
	public static final String TERM_MIN_LEN = "term_min_len";
	
	public static final String DIRECT_WEIGHT = "wdirect";
	public static final String INDIRECT_WEIGHT = "windirect";
	public static final String CONTROL_WEIGHT = "wcontrol";
	public static final String PII_WEIGHT = "wpii";
	public static final String NVD_WEIGHT = "wnvd";
	
	public static final String NVD_MAX_COUNT = "nvdmax";				//maximum value set manually to calculate the ratio for nvd terms
	public static final String PII_MAX_COUNT = "piimax";				//maximum value set manually to calculate the ratio for pii terms
	public static final String DIRECT_MAX_COUNT = "directmax";			//maximum value set manually to calculate the ratio for direct terms
	public static final String CONTROL_MAX_COUNT = "controlmax";		//maximum value set manually to calculate the ratio for control terms
	public static final String INDIRECT_MAX_COUNT = "indirectmax";		//maximum value set manually to calculate the ratio for indirect terms
	
	public static final String OPTION_YES = "option_yes";
	public static final String OPTION_NO = "option_no";
	public static final String OPTION_DK = "option_dk";
	
	public static final String Override_UserChanges = "override_user_changes";
	
	public static final String CWE_MAPPING = "cwe_mapping";
	
	//machine learning properties
	public static final String DATA_FILE = "data_file";
	public static final String TEXT_INDEX = "text_index";
	public static final String CLASS_INDEX = "class_index";
	public static final String SEPARATOR = "separator";
	public static final String HEADER = "header";
	public static final String ALGORITHM = "algorithm";
	public static final String TRAIN_SIZE = "train_size";
	public static final String NUM_EXPERIMENT = "num_expr";
	
	public static final String LEARNER_TSEARCH = "learner_or_term_search";
	
	//public static final String STORAGE_PATH = ComponentAccessor.getComponentOfType(JiraHome.class).getHome()+System.getProperty("file.separator")+"JiraSecPlugin"+System.getProperty("file.separator");
	public static final String FOLDER = "JiraSecPlugin";
	public static final String STORAGE_PATH_TEST = System.getProperty("user.dir")+System.getProperty("file.separator")+FOLDER+System.getProperty("file.separator");
	
	private PropertyKeys(){
		
	}
}

/**
 * 
 */
package no.tosin.oyetoyan.plugins;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import no.tosin.oyetoyan.plugins.PropertyKeys;
import no.tosin.oyetoyan.plugins.Validator;

/**
 * @author tosindo
 *
 */
public class SecurityImportance {
	
	private Properties properties;
	private SecurityKeyWords skw;
	private String importance = null; 			//use options in the property file
	
	public SecurityImportance(Properties prop, SecurityKeyWords skw){
		properties = prop;
		this.skw = skw;
	}
	
	public void compute(int piiCount, int controlCount, int directCount, int indirectCount){
		
		//compute score for the terms
		double piiScore = score(piiCount, Validator.getInt(4, properties.getProperty(PropertyKeys.PII_MAX_COUNT)));
		double directScore = score(directCount, Validator.getInt(1, properties.getProperty(PropertyKeys.DIRECT_MAX_COUNT)));
		double controlScore = score(controlCount, Validator.getInt(5, properties.getProperty(PropertyKeys.CONTROL_MAX_COUNT)));
		double indirectScore = score(indirectCount, Validator.getInt(5, properties.getProperty(PropertyKeys.INDIRECT_MAX_COUNT)));
		//double nvdScore = score(nvdCount, Validator.getInt(1, properties.getProperty(PropertyKeys.NVD_MAX_COUNT)));
		
		//get weights for control and indirect terms
		//double nvdWeight = Validator.getDouble(1.0, properties.getProperty(PropertyKeys.NVD_WEIGHT));
		double piiWeight = Validator.getDouble(0.4, properties.getProperty(PropertyKeys.PII_WEIGHT));
		double directWeight = Validator.getDouble(0.4, properties.getProperty(PropertyKeys.DIRECT_WEIGHT));
		double controlWeight = Validator.getDouble(0.1, properties.getProperty(PropertyKeys.CONTROL_WEIGHT));
		double indirectWeight = Validator.getDouble(0.1, properties.getProperty(PropertyKeys.INDIRECT_WEIGHT));
		
		//validate that sum of weights = 1.0
		Map<String, Double> defWeight = new HashMap<String, Double>();
		boolean weightsSumEqualOne = validateWeights(directWeight, indirectWeight, controlWeight, piiWeight, defWeight);
		if(!weightsSumEqualOne){
			directWeight = defWeight.get("dw");
			indirectWeight = defWeight.get("idw");
			controlWeight = defWeight.get("cw");
			piiWeight = defWeight.get("pw");
		}
		
		//compute final rank
		double rank = controlScore*controlWeight + indirectScore*indirectWeight + 
				piiScore*piiWeight + directScore*directWeight;	// + nvdScore*nvdWeight
		
		/*if(rank > 1.0)		//we have found published vulnerable product in the description
			rank = 1.0000;*/
		
		//format into 2 decimal places
		DecimalFormat toTwo = new DecimalFormat("0.00");
		rank = Double.valueOf(toTwo.format(rank));	
		
		//set rank and security related fields
		if(rank > 0.0){
			determineImportance(rank, skw.importanceLevels());			
		}else
			importance = skw.importanceLevels().get(0);		//set to NONE

	}
	
	private boolean validateWeights(double dw, double idw, double cw, double pw, Map<String, Double> defWeight){
		//is the sum equal 1.0? else reset to default weights
		Double sum = dw+idw+cw+pw;
		if(sum > 1.0 || sum < 1.0){
			defWeight.put("dw", 0.4);
			defWeight.put("cw", 0.1);
			defWeight.put("idw", 0.1);
			defWeight.put("pw", 0.4);
			
			return false;
		}else
			return true;
	}
	
	private double score(int count, int max){
		double score = 0.0;
		
		if(count > 0 && max > 0){	//avoid division by zero
			if(count >= max)
				score = 1;
			else
				score = (double)count/(double) max;
		}		
		
		return score;
		
	}
	
	private void determineImportance(double rank, Map<Integer, String> importance){
		double interval = (double) 1/(double) (importance.keySet().size()-1);

		Integer max = 0;
		for(int level : importance.keySet())
			max = Math.max(max, level);
		
		double lr = 0.0;
		for(int i=1; i<=max; i++){
			if(rank > lr && rank <= lr+interval){
				this.importance = importance.get(i);				
				break;
			}
			lr = lr + interval;
		}
		
	}

	/**
	 * @return the importance
	 */
	public String getImportance() {
		return importance;
	}
}

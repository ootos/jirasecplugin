/*
 * JiraSecPlugin 
 * Copyright 2016 Tosin Daniel Oyetoyan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package no.tosin.oyetoyan.plugins;

public class IssueData {
	
	private String issueLastSaved;
	private String importanceLastSaved;
	private int summaryLength;
	private int descriptionLength;
	
	public IssueData(String issue, String importance, int summaryLen, int descLen) {
		this.issueLastSaved = issue;
		this.importanceLastSaved = importance;
		this.summaryLength = summaryLen;
		this.descriptionLength = descLen;
	}

	public String getIssueLastSaved() {
		return issueLastSaved;
	}

	public void setIssueLastSaved(String issueLastSaved) {
		this.issueLastSaved = issueLastSaved;
	}

	public String getImportanceLastSaved() {
		return importanceLastSaved;
	}

	public void setImportanceLastSaved(String importanceLastSaved) {
		this.importanceLastSaved = importanceLastSaved;
	}

	public int getSummaryLength() {
		return summaryLength;
	}

	public void setSummaryLength(int summaryLength) {
		this.summaryLength = summaryLength;
	}

	public int getDescriptionLength() {
		return descriptionLength;
	}

	public void setDescriptionLength(int descriptionLength) {
		this.descriptionLength = descriptionLength;
	}

}

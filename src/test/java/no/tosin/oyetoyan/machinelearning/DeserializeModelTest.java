package no.tosin.oyetoyan.machinelearning;

import junit.framework.Assert;
import junit.framework.TestCase;
import no.tosin.oyetoyan.machinelearning.DeserializeModel;
import no.tosin.oyetoyan.plugins.SecurityKeyWords;

public class DeserializeModelTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public final void testClassifyText() {
		//classify new text
		String text = "Fix bug on projects visibility for external users;Since "
				+ "version 3.1.0-rc  my external users "
				+ "have no more visibility to their project. So  after check the code  it "
				+ "seems that project permissions are based on llx_socpeople.id rather "
				+ "than llx_user.id. My fix go in this way";
		String text1 = "Correct Bug 315;in Main.inc.php in left_menu and top_menu "
				+ "$hookmanager have to be new only of it not instanciate before. "
				+ "Other wise all hook define in the arry will be remove";
		String text2 = " R    Risk buffer";
		String text3 = "Create application with home-screen Set up project  Create MainActivty  Attach Navigation Drawer";
		String text4 = " CardAdminWebapp - Moifying product data on student causes Unknown error Story  modifying "
				+ "data related to the product causes Unknown error    Reproduce      run http                       "
				+ "card-admin-webapp     go to students     select student to edit     edit data related to product     "
				+ "save   Actual   unknown error popup  see enclosed avi    Expected      data saved correctly     "
				+ "if data can t be saved  error message should be more human friendly ";
		
		SecurityKeyWords skw = new SecurityKeyWords(false);
		DeserializeModel dm = DeserializeModel.getInstance(skw);
		dm.printModelPerformance();
		dm.printAttributes();
		
		String expected = dm.classifyText(text);
		String actual = "YES";
		String expected1 = dm.classifyText(text1);
		String actual1 = "NO";
		String expected2 = dm.classifyText(text2);
		String actual2 = "YES";
		String expected3 = dm.classifyText(text3);
		String actual3 = "NO";
		String expected4 = dm.classifyText(text4);
		String actual4 = "YES";
		
		//System.out.println(dm.getMessage());
		System.out.println(dm.getNumAttributes());
		System.out.println(dm.getNumTermFound());
		Assert.assertEquals(expected, actual);
		Assert.assertEquals(expected1, actual1);
		Assert.assertEquals(expected2, actual2);
		Assert.assertEquals(expected3, actual3);
		Assert.assertEquals(expected4, actual4);
	}

}

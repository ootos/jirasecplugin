package no.sintef.tosin.oyetoyan.plugins;

import junit.framework.TestCase;
import no.sintef.tosin.oyetoyan.plugins.SecurityKeyWords;

public class SecurityKeyWordsTest extends TestCase {
	
	private SecurityKeyWords skw;
	
	protected void setUp() throws Exception {
		//super.setUp();
		skw = new SecurityKeyWords(false);
	}

	protected void tearDown() throws Exception {
		//super.tearDown();
		skw = null;
	}

	public final void testControlTerms() {
		assertTrue(skw.controlTerms().size() > 0);
	}

	public final void testDirectTerms() {
		assertTrue(skw.directTerms().size() > 0);
	}

	public final void testIndirectTerms() {
		assertTrue(skw.indirectTerms().size() > 0);
	}

	public final void testPiiTerms() {
		assertTrue(skw.piiTerms().size() > 0);
	}
	
	public final void testNvdTerms() {
		assertTrue(skw.nvdTerms().size() > 0);
	}
	
	public final void testSeparators() {
		assertTrue(skw.separators().size() > 0);
	}
	
	public final void testStopWords() {
		assertTrue(skw.stopWords().size() > 0);
	}
	
	public final void testGetSecurityDescription() {
		assertTrue(skw.getSecurityDescription().size() > 0);
	}
	
	public final void testGetMessage() {
		assertTrue(skw.getMessage().size() > 0);
	}
	
	public final void testImportanceLevels() {
		assertTrue(skw.getMessage().size() > 0);
	}
	
	public final void testGetPropertyValues() {
		//test for the number of keys
		assertEquals(27, skw.getPropertyValues().size());
	}

}

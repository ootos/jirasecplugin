package no.sintef.tosin.oyetoyan.plugins;

import junit.framework.TestCase;
import junit.framework.Assert;
import no.sintef.tosin.oyetoyan.plugins.SecurityIssueClassifier;
import no.sintef.tosin.oyetoyan.plugins.SecurityKeyWords;

public class SecurityIssueClassifierTest extends TestCase {
	
	private String description;
	private String summary;
	
	private SecurityIssueClassifier sic;
	
	protected void setUp() throws Exception {
		description = "Can we use FTP for this purpose IllegalArgumentException?";
		summary = "Transfer log files for backup";
		sic = new SecurityIssueClassifier(new SecurityKeyWords(false));
		sic.classify(description, summary);
	}

	protected void tearDown() throws Exception {
		//super.tearDown();
		description = null;
		summary = null;
		sic = null;
	}

	public final void testIsSecurityRelated() {
		System.out.println(sic.isSecurityRelated()+" "+sic.getImportanceLevel()+" "+sic.getRank());
		printOutFoundTerms();
		assertEquals("YES", sic.isSecurityRelated());
	}

	public final void testGetImportanceLevel() {
		assertTrue(!sic.getImportanceLevel().equals("NONE"));
	}

	public final void testGetRank() {
		assertTrue(sic.getRank() > 0.0);
	}
	
	public final void testGetSecurityDescription() {
		assertNotNull(sic.getSecurityDescription());
	}
	
	public final void testGetSecurityMessage() {
		assertNotNull(sic.getSecurityMessage());
	}
	
	public final void testGetDirectFound() {
		Assert.assertTrue(sic.getDirectFound().size() == 0);
	}
	
	public final void testGetControlFound() {
		Assert.assertTrue(sic.getControlFound().size() > 0);
	}
	
	public final void testGetIndirecFound() {
		Assert.assertTrue(sic.getIndirectFound().size() > 0);
	}
	
	public final void testGetPIIFound() {
		Assert.assertTrue(sic.getPiiFound().size() == 0);
	}
	
	public final void testGetNVDFound() {
		Assert.assertTrue(sic.getNvdFound().size() == 0);
	}
	
	private void printOutFoundTerms(){
		System.out.println("Direct Terms Found");
		System.out.println(sic.getDirectFound());
		System.out.println("============");
		System.out.println("PII Terms Found");
		System.out.println(sic.getPiiFound());
		System.out.println("============");
		System.out.println("Control Terms Found");
		System.out.println(sic.getControlFound());
		System.out.println("============");
		System.out.println("Indirect Terms Found");
		System.out.println(sic.getIndirectFound());
		System.out.println("============");
		System.out.println("NVD Terms Found");
		System.out.println(sic.getNvdFound());
		System.out.println("============");
		System.out.println(sic.getSecurityMessage());
	}

}
